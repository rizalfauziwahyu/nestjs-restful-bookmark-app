import {
  Injectable,
  ForbiddenException,
} from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma/prisma.service';
import { EditUserDto } from './dto';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async editUser(
    userId: number,
    dto: EditUserDto,
  ) {
    try {
      const user: User =
        await this.prisma.user.findUnique({
          where: { id: userId },
        });
      if (!user)
        throw new ForbiddenException(
          "User does'nt exists",
        );

      const updateUser: User =
        await this.prisma.user.update({
          where: {
            id: userId,
          },
          data: { ...dto },
        });

      delete updateUser.hash;

      return updateUser;
    } catch (err) {
      if (
        err instanceof
        PrismaClientKnownRequestError
      ) {
        if (err.code === 'P2002') {
          throw new ForbiddenException(
            'User email is already taken',
          );
        }
      }
      throw err;
    }
  }
}
