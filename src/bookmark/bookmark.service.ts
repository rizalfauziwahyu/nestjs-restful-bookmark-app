import {
  Injectable,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';
import { Bookmark } from '@prisma/client';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  CreateBookmarkDto,
  EditBookmarkDto,
} from './dto';

@Injectable()
export class BookmarkService {
  constructor(private prisma: PrismaService) {}

  async createBookmark(
    userId: number,
    dto: CreateBookmarkDto,
  ): Promise<Bookmark> {
    try {
      const bookmark =
        await this.prisma.bookmark.create({
          data: {
            ...dto,
            userId: userId,
          },
          include: {
            user: {
              select: {
                email: true,
              },
            },
          },
        });

      return bookmark;
    } catch (err) {
      throw err;
    }
  }

  async getBookmarks(
    userId: number,
  ): Promise<Bookmark[]> {
    try {
      const bookmarks =
        await this.prisma.bookmark.findMany({
          where: {
            userId: userId,
          },
        });

      return bookmarks;
    } catch (err) {
      throw err;
    }
  }

  async getBookmarkById(
    userId: number,
    bookmarkId: number,
  ): Promise<Bookmark> {
    try {
      const bookmark: Bookmark =
        await this.prisma.bookmark.findUnique({
          where: {
            id: bookmarkId,
          },
        });

      if (!bookmark)
        throw new NotFoundException(
          'this resource does not exist',
        );

      if (bookmark.userId !== userId)
        throw new ForbiddenException(
          'access to this resource denied',
        );

      return bookmark;
    } catch (err) {
      if (
        err instanceof
        PrismaClientKnownRequestError
      )
        if (err.code === 'P2001')
          throw new ForbiddenException(
            'The bookmark record does not exist',
          );

      throw err;
    }
  }

  async editBookmarkById(
    userId: number,
    bookmarkId: number,
    dto: EditBookmarkDto,
  ): Promise<Bookmark> {
    try {
      const bookmark =
        await this.prisma.bookmark.findUnique({
          where: {
            id: bookmarkId,
          },
        });

      if (!bookmark)
        throw new NotFoundException(
          'this resource does not exist',
        );

      // check if user owns the bookmark
      if (bookmark.userId !== userId)
        throw new ForbiddenException(
          'Access to resources denied',
        );

      return await this.prisma.bookmark.update({
        where: {
          id: bookmarkId,
        },
        data: {
          ...dto,
        },
      });
    } catch (err) {
      if (
        err instanceof
        PrismaClientKnownRequestError
      ) {
        if (err.code === 'P2001')
          throw new ForbiddenException(
            'The bookmark record does not exist',
          );
      }
      throw err;
    }
  }

  async deleteBookmarkById(
    userId: number,
    bookmarkId: number,
  ): Promise<Bookmark> {
    try {
      const bookmark =
        await this.prisma.bookmark.findUnique({
          where: {
            id: bookmarkId,
          },
        });

      if (!bookmark)
        throw new NotFoundException(
          'this resource does not exist',
        );

      if (bookmark.userId !== userId)
        throw new ForbiddenException(
          'Access to this resource denied',
        );

      return await this.prisma.bookmark.delete({
        where: {
          id: bookmarkId,
        },
      });
    } catch (err) {
      if (
        err instanceof
        PrismaClientKnownRequestError
      )
        if (err.code === 'P2001')
          throw new ForbiddenException(
            'The bookmark record does not exist',
          );

      throw err;
    }
  }
}
